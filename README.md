# tol-patch-parser

![Screenshot](https://i.imgur.com/vK1Ex4M.png)

Isn't it annoying to have to parse your patch notes multiple times between your Trello/Discord/Discourse (richTxt), Steam (bb) and in-game (TextMeshPro / Custom)?

Use this as a template to customize for your needs. It can surely be improved: Fork away!

![Screenshot](https://i.imgur.com/TCo8TuK.png)

## Features
* H1
* H2
* Quotes
* Ordered Lists
* Unordered Lists
* Bold
* Italics

## TODO
* Links
* Images
* UI Polish
* Copy buttons
* Init with template changelog.txt

## Shameless plug
Found this useful? Support Throne of Lies on Steam @ http://store.steampowered.com/app/595280

## Questions?
Xblade#4242 @ https://discord.gg/tol

## Attributions
* Scroll icon by https://www.flaticon.com/authors/icongeek26

## License
MIT