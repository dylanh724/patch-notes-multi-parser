﻿namespace PatchParser
{
    partial class PatchParserForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.parseBtn = new System.Windows.Forms.Button();
            this.markdownTxt = new System.Windows.Forms.TextBox();
            this.bbTxt = new System.Windows.Forms.TextBox();
            this.inGameTxt = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(261, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Src Markdown (Trello/Discourse/Discord) >>";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(500, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "BBCode (Steam)";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(983, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "In-Game (TMPro)";
            // 
            // parseBtn
            // 
            this.parseBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.parseBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.parseBtn.Location = new System.Drawing.Point(14, 737);
            this.parseBtn.Name = "parseBtn";
            this.parseBtn.Size = new System.Drawing.Size(480, 35);
            this.parseBtn.TabIndex = 3;
            this.parseBtn.Text = "Parse from Markdown >>";
            this.parseBtn.UseVisualStyleBackColor = true;
            this.parseBtn.Click += new System.EventHandler(this.parseBtn_Click);
            // 
            // markdownTxt
            // 
            this.markdownTxt.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.markdownTxt.BackColor = System.Drawing.SystemColors.Info;
            this.markdownTxt.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.markdownTxt.Location = new System.Drawing.Point(14, 25);
            this.markdownTxt.Multiline = true;
            this.markdownTxt.Name = "markdownTxt";
            this.markdownTxt.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.markdownTxt.Size = new System.Drawing.Size(480, 701);
            this.markdownTxt.TabIndex = 0;
            this.markdownTxt.WordWrap = false;
            // 
            // bbTxt
            // 
            this.bbTxt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bbTxt.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bbTxt.Location = new System.Drawing.Point(500, 25);
            this.bbTxt.Multiline = true;
            this.bbTxt.Name = "bbTxt";
            this.bbTxt.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.bbTxt.Size = new System.Drawing.Size(480, 701);
            this.bbTxt.TabIndex = 1;
            this.bbTxt.WordWrap = false;
            // 
            // inGameTxt
            // 
            this.inGameTxt.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.inGameTxt.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inGameTxt.Location = new System.Drawing.Point(986, 25);
            this.inGameTxt.Multiline = true;
            this.inGameTxt.Name = "inGameTxt";
            this.inGameTxt.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.inGameTxt.Size = new System.Drawing.Size(480, 701);
            this.inGameTxt.TabIndex = 2;
            this.inGameTxt.WordWrap = false;
            // 
            // PatchParserForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1484, 784);
            this.Controls.Add(this.parseBtn);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.inGameTxt);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.bbTxt);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.markdownTxt);
            this.Name = "PatchParserForm";
            this.Text = "Patch Parser";
            this.Load += new System.EventHandler(this.PatchParserForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button parseBtn;
        private System.Windows.Forms.TextBox markdownTxt;
        private System.Windows.Forms.TextBox bbTxt;
        private System.Windows.Forms.TextBox inGameTxt;
    }
}

