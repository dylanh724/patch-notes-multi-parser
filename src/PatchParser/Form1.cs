﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PatchParser
{
    public partial class PatchParserForm : Form
    {
        const string PATH_TO_CHANGELOG = @"path/to/changelog.txt"; // TODO

        // #################################################
        // From markdown src
        // TODO: Get gud at regex
        // #################################################
        const string H1_PATTERN = @"(^#\s)(.+?)(\r\n)";
        const string H2_PATTERN = @"(^##\s)(.+?)(\r\n)";
        const string QUOTE_PATTERN = @"(^>\s)(.+?)(\r\n)";
        const string UNORDERED_LIST_PATTERN = @"(^\*\s)(.+?)(\r\n)";
        const string BOLD_PATTERN = @"(\*\*)(\S.+?\S)(\*\*)";

        // Must do this AFTER bold 
        // TODO, test:  @"\*([^\r\n\t\v *]+)\*(?!\*)"
        const string ITALICS_PATTERN = @"(\*)(\S+\S)(\*)";

        /// <summary>Optional newline ending!</summary>
        const string ORDERED_LIST_PATTERN = @"(^\d\.\s)(.+?)(\r?\n?)";

        // #################################################
        // Custom tmp
        // #################################################
        // "<indent=0%><size=46><color=#ffa500ff> <u>Bug Fixes</u>:</color></size>
        const string H1_TMP_PREFIX = "<indent=0%><size=46><color=#ffa500ff> <u>";
        const string H1_TMP_SUFFIX = "</u>:</color></size>";

        // QUOTE==H2
        // <indent=3%> <size=36><color=#ffa500ff> <u>Class Fixes</u>:</color></size>
        const string H2_QUOTE_TMP_PREFIX = "<indent=3%> <size=36><color=#ffa500ff> <u>";
        const string H2_QUOTE_TMP_SUFFIX = "</u>:</color></size>";

        // <indent=4%> - <indent=6%>Added ability for devs to swap regions without a patch if servers go unstable.
        const string TMP_UOLIST_PREFIX = "<indent=4%> - <indent=6%>";
        const string TMP_OLIST_PREFIX_1 = "<indent=4%> ";
        const string TMP_OLIST_PREFIX_2 = "<indent=6%>";

        // ....................................................................
        public PatchParserForm()
        {
            InitializeComponent();
        }

        // ....................................................................
        private void parseBtn_Click(object sender, EventArgs e)
        {
            parseTxt();
        }

        // ....................................................................
        private void parseTxt()
        {
            // Get src and split it into lines
            string markdownSrc = markdownTxt.Text;

            // ---------------------------
            string bbStr = parseMarkdownTxtToBBViaRegex(markdownSrc);
            string inGameStr = parseMarkdownTxtToTMProViaRegex(markdownSrc);

            bbTxt.Text = bbStr;
            inGameTxt.Text = inGameStr;

            // Scroll to top
            markdownTxt.SelectionStart = 0;
            markdownTxt.ScrollToCaret();

            bbTxt.SelectionStart = 0;
            bbTxt.ScrollToCaret();

            inGameTxt.SelectionStart = 0;
            inGameTxt.ScrollToCaret();
        }

        // ....................................................................
        private string parseMarkdownTxtToBBViaRegex(string _richSrcStr)
        {
            string bbStr = _richSrcStr;

            bbStr = Regex.Replace(bbStr, H1_PATTERN, onH1RegexToBB, RegexOptions.Multiline);
            bbStr = Regex.Replace(bbStr, H2_PATTERN, onH2RegexToBB, RegexOptions.Multiline);
            bbStr = Regex.Replace(bbStr, QUOTE_PATTERN, onQuoteRegexToBB, RegexOptions.Multiline);
            bbStr = Regex.Replace(bbStr, BOLD_PATTERN, onBoldRegexToBB, RegexOptions.Multiline); // Always before italics
            bbStr = Regex.Replace(bbStr, ITALICS_PATTERN, onItalicsRegexToBB, RegexOptions.Multiline); // Always after bold
            bbStr = Regex.Replace(bbStr, UNORDERED_LIST_PATTERN, onUOListRegexToBB, RegexOptions.Multiline);
            bbStr = addUnorderedListStartEndTags(bbStr);
            bbStr = addOrderedListStartEndTags(bbStr);
            bbStr = Regex.Replace(bbStr, ORDERED_LIST_PATTERN, onOListRegexToBB, RegexOptions.Multiline); // Always after UO+tags
            
            return bbStr;
        }

        // ....................................................................
        private string parseMarkdownTxtToTMProViaRegex(string _richSrcStr)
        {
            string tmpStr = _richSrcStr;

            tmpStr = Regex.Replace(tmpStr, H1_PATTERN, onH1RegexToTMPro, RegexOptions.Multiline);
            tmpStr = Regex.Replace(tmpStr, H2_PATTERN, onH2RegexToTMPro, RegexOptions.Multiline);
            tmpStr = Regex.Replace(tmpStr, QUOTE_PATTERN, onQuoteRegexToTMPro, RegexOptions.Multiline);
            tmpStr = Regex.Replace(tmpStr, BOLD_PATTERN, onBoldRegexToTMPro, RegexOptions.Multiline); // Always before italics
            tmpStr = Regex.Replace(tmpStr, ITALICS_PATTERN, onItalicsRegexToTMPro, RegexOptions.Multiline); // Always after bold
            tmpStr = Regex.Replace(tmpStr, UNORDERED_LIST_PATTERN, onUOListRegexToTMPro, RegexOptions.Multiline);
            //tmpStr = addUnorderedListStartEndTags(tmpStr);
            //tmpStr = addOrderedListStartEndTags(tmpStr);
            tmpStr = Regex.Replace(tmpStr, ORDERED_LIST_PATTERN, onOListRegexToTMPro, RegexOptions.Multiline); // Always after UO+tags

            return tmpStr;
        }

        // ....................................................................
        private string addOrderedListStartEndTags(string str)
        {
            StringBuilder strB = new StringBuilder();
            string line = "";
            bool startedList = false, startsWithNumPeriodSpace = false;

            using (System.IO.StringReader reader = new System.IO.StringReader(str))
            {
                while (reader.Peek() > 0)
                {
                    // Read next line
                    line = reader.ReadLine();

                    // Does this contain "#. "?
                    startsWithNumPeriodSpace = Regex.IsMatch(line, ORDERED_LIST_PATTERN);

                    if (!startedList && startsWithNumPeriodSpace)
                    {
                        startedList = true;
                        strB.AppendLine("[olist]");
                    }
                    else if (startedList && !startsWithNumPeriodSpace)
                    {
                        startedList = false;
                        strB.AppendLine("[/olist]");
                    }

                    strB.AppendLine(line);
                }
            }

            return strB.ToString();
        }

        // ....................................................................
        private string addUnorderedListStartEndTags(string str)
        {
            StringBuilder strB = new StringBuilder();
            string line = "";
            bool startedList = false, startsWithBullet = false;

            using (System.IO.StringReader reader = new System.IO.StringReader(str))
            {
                while (reader.Peek() > 0)
                {
                    // Read next line
                    line = reader.ReadLine();

                    // Does this contain "[*]"?
                    startsWithBullet = line.StartsWith("[*]");

                    if (!startedList && startsWithBullet)
                    {
                        startedList = true;
                        strB.AppendLine("[list]");
                    }
                    else if (startedList && !startsWithBullet)
                    {
                        startedList = false;
                        strB.AppendLine("[/list]");
                    }

                    strB.AppendLine(line);
                }
            }

            return strB.ToString();
        }

        // ....................................................................
        private string onOListRegexToTMPro(Match m)
        {
            string val1 = m.Groups[1].Value;
            string val2 = m.Groups[2].Value;
            string val3 = m.Groups[3].Value;

            string res = string.Format(
                "{0}{1}{2}",
                TMP_OLIST_PREFIX_1 + val1 + TMP_OLIST_PREFIX_2,
                val2,
                val3
            );

            return res;
        }

        // ....................................................................
        private string onOListRegexToBB(Match m)
        {
            string val1 = m.Groups[1].Value;
            string val2 = m.Groups[2].Value;
            string val3 = m.Groups[3].Value;

            string res = string.Format(
                "{0}{1}{2}",
                val1 = "[*]",
                val2,
                val3
            );

            return res;
        }

        // ....................................................................
        private string onUOListRegexToTMPro(Match m)
        {
            string val1 = m.Groups[1].Value;
            string val2 = m.Groups[2].Value;
            string val3 = m.Groups[3].Value;

            string res = string.Format(
                "{0}{1}{2}",
                val1.Replace("* ", TMP_UOLIST_PREFIX),
                val2,
                val3
            );

            return res;
        }

        // ....................................................................
        private string onUOListRegexToBB(Match m)
        {
            string val1 = m.Groups[1].Value;
            string val2 = m.Groups[2].Value;
            string val3 = m.Groups[3].Value;

            string res = string.Format(
                "{0}{1}{2}",
                val1.Replace("* ", "[*]"),
                val2,
                val3
            );

            return res;
        }

        // ....................................................................
        private string onItalicsRegexToTMPro(Match m)
        {
            string val1 = m.Groups[1].Value;
            string val2 = m.Groups[2].Value;
            string val3 = m.Groups[3].Value;

            string res = string.Format(
                "{0}{1}{2}",
                val1.Replace("*", "<i>"),
                val2,
                val1.Replace("*", "</i>")
            );

            return res;
        }

        // ....................................................................
        private string onItalicsRegexToBB(Match m)
        {
            string val1 = m.Groups[1].Value;
            string val2 = m.Groups[2].Value;
            string val3 = m.Groups[3].Value;

            string res = string.Format(
                "{0}{1}{2}",
                val1.Replace("*", "[i]"),
                val2,
                val1.Replace("*", "[/i]")
            );

            return res;
        }

        // ....................................................................
        private string onBoldRegexToTMPro(Match m)
        {
            string val1 = m.Groups[1].Value;
            string val2 = m.Groups[2].Value;
            string val3 = m.Groups[3].Value;

            string res = string.Format(
                "{0}{1}{2}",
                val1.Replace("**", "<b>"),
                val2,
                val1.Replace("**", "</b>")
            );

            return res;
        }

        // ....................................................................
        private string onBoldRegexToBB(Match m)
        {
            string val1 = m.Groups[1].Value;
            string val2 = m.Groups[2].Value;
            string val3 = m.Groups[3].Value;

            string res = string.Format(
                "{0}{1}{2}",
                val1.Replace("**", "[b]"),
                val2,
                val1.Replace("**", "[/b]")
            );

            return res;
        }

        // ....................................................................
        private string onQuoteRegexToBB(Match m)
        {
            string val1 = m.Groups[1].Value;
            string val2 = m.Groups[2].Value;
            string val3 = m.Groups[3].Value;

            string res = string.Format(
                "{0}{1}{2}",
                val1.Replace("> ", "[quote]" + Environment.NewLine),
                val2,
                $"{Environment.NewLine}[/quote]{val3}"
            );

            return res;
        }

        // ....................................................................
        // There's no good h2 in Steam's bb, so let's just do bold
        // Idea: +Optionally forced caps?
        private string onH2RegexToBB(Match m)
        {
            string val1 = m.Groups[1].Value;
            string val2 = m.Groups[2].Value;
            string val3 = m.Groups[3].Value;

            string res = string.Format(
                "{0}{1}{2}",
                val1.Replace("## ", "[b]"),
                val2,
                $"[/b]{val3}"
            );

            return res;
        }

        // ....................................................................
        private string onH1RegexToBB(Match m)
        {
            string val1 = m.Groups[1].Value;
            string val2 = m.Groups[2].Value;
            string val3 = m.Groups[3].Value;

            string res = string.Format(
                "{0}{1}{2}",
                val1.Replace("# ", "[h1]"),
                val2,
                $"[/h1]{val3}"
            );

            return res;
        }

        // ....................................................................
        private string onH1RegexToTMPro(Match m)
        {
            string val1 = m.Groups[1].Value;
            string val2 = m.Groups[2].Value;
            string val3 = m.Groups[3].Value;

            string res = string.Format(
                "{0}{1}{2}",
                val1.Replace("# ", H1_TMP_PREFIX),
                val2,
                H1_TMP_SUFFIX
            );

            return res;
        }

        // ....................................................................
        private string onH2RegexToTMPro(Match m)
        {
            string val1 = m.Groups[1].Value;
            string val2 = m.Groups[2].Value;
            string val3 = m.Groups[3].Value;

            string res = string.Format(
                "{0}{1}{2}",
                val1.Replace("## ", H2_QUOTE_TMP_PREFIX),
                val2,
                H2_QUOTE_TMP_SUFFIX
            );

            return res;
        }

        // ....................................................................
        private string onQuoteRegexToTMPro(Match m)
        {
            string val1 = m.Groups[1].Value;
            string val2 = m.Groups[2].Value;
            string val3 = m.Groups[3].Value;

            string res = string.Format(
                "{0}{1}{2}",
                val1.Replace("> ", H2_QUOTE_TMP_PREFIX),
                val2,
                H2_QUOTE_TMP_SUFFIX
            );

            return res;
        }

        // ....................................................................
        private void PatchParserForm_Load(object sender, EventArgs e)
        {
            // TODO: Preload from a changelog file?
        }
    }
}
